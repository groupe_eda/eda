# Système de jeu

Quand on charge pour la première fois, on met en place en json avec l'uuid du match, on render dans un état initiale la map puis on commence le jeu. vous avez exactement 180 secondes pour coder le comportement de votre robot dans le
langage que nous avons créer : EDA#, une fois la fin du temps imparti, ou si les deux joueurs ont finit leur code en
avance, les robots exécutent le code. Les robots ont alors deux objectif qui définira le vainqueur, soit détruire le
robot adverse avec l'action "shield", qui fait exploser l'adversaire si il rentre dans la zone de couverture ( qui
est de un bloc autour du robot qui utilise "shield"), soit récupérer le plus de pièces possible en marchant dessus.
Après que les deux robots ont finit d'exécuter leurs instructions respective, le vainqueur sera le dernier robot en
vie, ou, dans le cas où les deux ont survécu, celui qui aura amassé le plus de pièces durant la partie. Vous serez
ensuite redirigé vers l'écran de fin de partie annonçant le gagnant.