# Protocole d'utilisation

LE PROJET EST HOST SUR LE SITE SUIVANT: http://163.5.143.223:1234
## 1. Installation
Lorsque vous avez installé les fichiers dans le dossier source, et que vous avez installé les dépendances. Il y a deux moyens d'executé et de lancer le serveur

## 2. Lancement de test
Vous pouvez lancer le serveur en executant simplement le server.py, cela va lancer un serveur de test, ceci marche pour linux et Windows.

## 3. Lancer un serveur de production
Pour lancer un serveur de production, il faut suivre la [documentation officiel de flask](https://flask.palletsprojects.com/en/3.0.x/deploying/).

Nous allons vous detailler comment le faire avec gunicorn

### 1 - Ouvrir une console de commande et aller dans votre repertoire avec votre server.py

### 2 - Installation de venv
#### 2.1 - Linux
Pour Debian, utiliser "sudo apt update" puis "sudo apt install python3.X-venv", X étant votre version python (Exemple X= 10, python3.10).
Pour les autres distributions, le nom devrait être le même, changer juste "apt" par votre package manager.

#### 2.2 - Windows
Executer la commande "pip install virtualenv", puis allez dans le repertoire Scripts "cd C:\Python27\Scripts" Créé un venv avec "python virtualenv.exe my_env"

### 3 - Lancement du venv
Pour linux, dès que c'est fait, executer "python -m venv .venv" puis ". .venv/bin/activate"
Pour windows, executer "my_env\Scripts\activate.bat"

## Lancement du serveur
Installer gunicorn dans le venv avec "pip install gunicorn" ainsi que flask avec "pip install flask"
Dès que c'est fait, executer la commande "gunicorn -w 4 'hello:app"